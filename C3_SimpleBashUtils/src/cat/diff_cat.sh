#!/bin/bash
expression=""
result=""

flags=("-b" "-e" "-n" "-s" "-t" "-v")
extra_flags=("--number" "--number-nonblank" "--squeeze-blank")
files=("test_1_cat.txt" "test_2_cat.txt" "test_3_cat.txt" "test_4_cat.txt" "test_5_cat.txt" "test_case_cat.txt" "null_file.txt")

current_number=1;
fail_number=0;
succes_number=0;

#[current_number|succes|fail] commands result;

tests without flags
echo "Tests without flags"
for i in ${files[@]}; do
    ./s21_cat $i > output.txt
    cat $i > output_cat.txt
    expression="$(diff output.txt output_cat.txt)"
    if [ "$expression" = "" ]
        then
            result="Success"
            succes_number=$(($succes_number + 1))
        else
            result="Fail"
            fail_number=$(($fail_number + 1))
        fi 
        echo "[$current_number|$succes_number|$fail_number] $result cat $i;"
        current_number=$(($current_number + 1))
done

echo ""

# tests with flags
echo "Tests with flags"
for i in ${flags[@]}; do
    for j in ${files[@]}; do
        ./s21_cat $i $j > output.txt
        cat $i $j > output_cat.txt
        expression="$(diff output.txt output_cat.txt)"
        if [ "$expression" = "" ]
        then
            result="Success"
            succes_number=$(($succes_number + 1))
        else
            result="Fail"
            fail_number=$(($fail_number + 1))
        fi
            echo "[$current_number|$succes_number|$fail_number] $result cat $i $j;"
            current_number=$(($current_number + 1))
    done
done

echo ""

# tests with 2 flags
echo "Tests with 2 flags"
for i in ${flags[@]}; do
    for j in ${flags[@]}; do
        for k in ${files[@]}; do
            if [ "$i" != "$j" ]
            then
                ./s21_cat $i $j $k > output.txt
                cat $i $j $k > output_cat.txt
                expression="$(diff output.txt output_cat.txt)"
                if [ "$expression" = "" ]
                then
                    result="Success"
                    succes_number=$(($succes_number + 1))
                else
                    result="Fail"
                    fail_number=$(($fail_number + 1))
                fi
                    echo "[$current_number|$succes_number|$fail_number] $result cat $i $j $k;"
                    current_number=$(($current_number + 1))
            fi
        done
    done
done

echo ""


# multiflags tests
echo "Tests with 5 flags"
for i in ${flags[@]}; do
    for j in ${flags[@]}; do
        for k in ${flags[@]}; do
            for l in ${flags[@]}; do
                for m in ${flags[@]}; do
                    for n in ${files[5]}; do
                            if [ "$i" != "$j" ] && [ "$i" != "$k" ] && [ "$j" != "$k" ] && [ "$i" != "$l" ] && [ "$j" != "$l" ] && [ "$k" != "$l" ] && [ "$i" != "$m" ] && [ "$j" != "$m" ] && [ "$k" != "$m" ] && [ "$l" != "$m" ]
                            then
                                ./s21_cat $i $j $k $l $m $n > output.txt
                                cat $i $j $k $l $m $n > output_cat.txt
                                expression="$(diff output.txt output_cat.txt)"
                                if [ "$expression" = "" ]
                                then
                                    result="Success"
                                    succes_number=$(($succes_number + 1))
                                else
                                    result="Fail"
                                    fail_number=$(($fail_number + 1))
                                fi
                                echo "[$current_number|$succes_number|$fail_number] $result cat $i $j $k $l $m $n"
                                current_number=$(($current_number + 1))
                            fi    
                    done
                done
            done        
        done
    done
done


echo "All: $(($current_number - 1)) | Success: $succes_number | Fail: $fail_number"

rm -rf output_cat.txt
rm -rf output.txt 