#ifndef s21_cat
#define s21_cat
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct options {
  int b;
  int e;
  int n;
  int s;
  int t;
  int v;
} opt;

void catParseArg(int argc, char *argv[], opt *options);
void handleFile(int argc, char *argv[], opt *options);
void catReader(FILE *f, opt *options);

#endif