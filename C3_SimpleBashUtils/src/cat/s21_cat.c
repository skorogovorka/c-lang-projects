#include "s21_cat.h"

int main(int argc, char *argv[]) {
  opt options = {0};
  catParseArg(argc, argv, &options);
  if (options.b) {
    options.n = 0;
  }
  handleFile(argc, argv, &options);
  return 0;
}

void catParseArg(int argc, char *argv[], opt *options) {
  int opt;
  int option_index;
  static struct option long_options[] = {{"number", 0, 0, 'n'},
                                         {"number-nonblank", 0, 0, 'b'},
                                         {"squeeze-blank", 0, 0, 's'},
                                         {0, 0, 0, 0}};
  while ((opt = getopt_long(argc, argv, "+benstvTE", long_options,
                            &option_index)) != -1) {
    switch (opt) {
      case 'b':
        options->b = 1;
        break;
      case 'e':
        options->e = 1;
        options->v = 1;
        break;
      case 'n':
        options->n = 1;
        break;
      case 's':
        options->s = 1;
        break;
      case 't':
        options->t = 1;
        options->v = 1;
        break;
      case 'v':
        options->v = 1;
        break;
      default:
        fprintf(stderr, "usage");
        exit(1);
    }
  }
}
void handleFile(int argc, char *argv[], opt *options) {
  for (int i = optind; i < argc; i++) {
    FILE *f = fopen(argv[i], "r");
    if (f) {
      catReader(f, options);
      fclose(f);
    } else {
      fprintf(stderr, "No such file or directory: %s\n", argv[i]);
    }
  }
}
void catReader(FILE *f, opt *options) {
  char c, prev_c = '\n';
  int line_number = 0, counter = 0, print = 0;
  while ((c = fgetc(f)) != EOF) {
    if (options->s) {
      if (c == '\n' && prev_c == '\n') {
        counter++;
        if (counter > 1) {
          print = 1;
        }
      } else {
        counter = 0;
      }
    }
    if (options->n) {
      if (counter <= 1) {
        if (prev_c == '\n') {
          line_number++;
          printf("%6d\t", line_number);
        }
      }
    }
    if (options->b && (prev_c == '\n' && (prev_c != c))) {
      line_number++;
      printf("%6d\t", line_number);
    }
    if (options->e) {
      if (counter <= 1) {
        if (c == '\n') {
          printf("$");
        }
      }
    }
    if (options->t) {
      if (c == '\t') {
        printf("^I");
        print = 1;
      }
    }
    if (options->v && ((c != 9 && c != 10 && c < 32) || c == 127)) {
      print = 1;
      if (c == 127) {
        printf("^?");
      } else {
        printf("^%c", c + 64);
      }
    }
    if (!print) {
      putchar(c);
    }
    prev_c = c;
    print = 0;
  }
}
