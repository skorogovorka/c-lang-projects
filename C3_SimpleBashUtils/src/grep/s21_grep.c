#include "s21_grep.h"

int main(int argc, char *argv[]) {
  char pattern[4096] = {0};
  opt options = {0};
  grepParseArg(argc, argv, &options, pattern);
  grepHandleFile(argc, argv, pattern, &options);
  return 0;
}

void grepParseArg(int argc, char *argv[], opt *options, char *pattern) {
  int opt;
  while ((opt = getopt_long(argc, argv, "e:ivclnhsf:o", NULL, 0)) != -1) {
    switch (opt) {
      case 'e':
        options->e = 1;
        flagsGrepE(optarg, pattern, options);
        break;
      case 'i':
        options->i = 1;
        break;
      case 'v':
        options->v = 1;
        break;
      case 'c':
        options->c = 1;
        break;
      case 'l':
        options->l = 1;
        break;
      case 'n':
        options->n = 1;
        break;
      case 'h':
        options->h = 1;
        break;
      case 's':
        options->s = 1;
        break;
      case 'f':
        options->f = 1;
        flagsGrepF(pattern, options);
        break;
      case 'o':
        options->o = 1;
        break;
      default:
        fprintf(stderr, "usage");
        exit(1);
    }
  }
  if (options->off_o == 1) options->o = 0;
}
void grepHandleFile(int argc, char *argv[], char *pattern, opt *options) {
  if (argc < 3 || optind == argc) {
    fprintf(stderr, "Usage: %s pattern file\n", argv[0]);
  } else {
    int current_file = 0;
    int files = 0;
    if (!options->e && !options->f) {
      current_file = optind + 1;
      strcat(pattern, argv[optind]);
    } else {
      current_file = optind;
    }
    files = (argc - current_file != 1) ? 1 : 0;
    regex_t regex = {0};
    if (compileRegex(pattern, options, &regex)) {
      for (int i = current_file; i < argc; i++) {
        processFile(argv[i], options, &regex, files);
      }
    }
    regfree(&regex);
  }
}

int compileRegex(char *pattern, opt *options, regex_t *regex) {
  int rgcmp = 1;
  if (options->i) {
    if (regcomp(regex, pattern, REG_EXTENDED | REG_ICASE | REG_NEWLINE) != 0) {
      fprintf(stderr, "Failed to compile regular expression\n");
      rgcmp = 0;
    }
  } else if (regcomp(regex, pattern, REG_EXTENDED | REG_NEWLINE) != 0) {
    fprintf(stderr, "Failed to compile regular expression\n");
    rgcmp = 0;
  }
  return rgcmp;
}

void processFile(char *filename, opt *options, regex_t *regex, int files) {
  FILE *file = fopen(filename, "r");
  if (file == NULL) {
    if (!options->s) {
      fprintf(stderr, "Failed to open file: %s\n", filename);
    }
  } else {
    char line[MAXLINE];
    int line_num = 0;
    int counetr_line = 0;
    while (fgets(line, MAXLINE, file) != NULL) {
      line_num++;
      if (compareFinding(regex, options, line)) {
        if (options->c || options->l) {
          counetr_line++;
        } else {
          if (files && (!options->h)) {
            printf("%s:", filename);
          }
          if (options->n) printf("%d:", line_num);
          if (options->o && !options->v) {
            flagsGrepO(line, regex);
          } else {
            printf("%s", line);
            if (strchr(line, '\n') == NULL) printf("\n");
          }
        }
      }
    }
    if (options->c || options->l) {
      if (files && (!options->h) && options->c) {
        printf("%s:", filename);
      }
    }
    if (options->c) {
      if (options->l && counetr_line > 0) counetr_line = 1;
      printf("%d\n", counetr_line);
    }
    if (options->l && counetr_line != 0) {
      printf("%s\n", filename);
    }
    fclose(file);
  }
}

int compareFinding(regex_t *regex, opt *options, char *line) {
  int str_match = 0;
  if (options->v) {
    if (regexec(regex, line, 0, NULL, 0) == 1) {
      str_match = 1;
    }
  } else if (regexec(regex, line, 0, NULL, 0) == 0) {
    str_match = 1;
  }
  return str_match;
}

void flagsGrepE(char *optarg, char *pattern, opt *options) {
  if (options->e) {
    if (strlen(pattern) > 0) strcat(pattern, "|");
    strcat(pattern, optarg);
  }
}

void flagsGrepF(char *pattern, opt *options) {
  FILE *f = fopen(optarg, "r");
  if (f == NULL) {
    fprintf(stderr, "Failed to open file: %s\n", optarg);
  } else {
    char buffer[4096];
    while (fgets(buffer, 4095, f) != NULL) {
      if (buffer[strlen(buffer) - 1] == '\n') {
        buffer[strlen(buffer) - 1] = '\0';
      }
      if (buffer[0] != '\0') {
        if (strlen(pattern) > 0) strcat(pattern, "|");
        strcat(pattern, buffer);
      }
      if (buffer[0] == '\0') {
        if (strlen(pattern) > 0) strcat(pattern, "|");
        strcat(pattern, "\n");
        strcat(pattern, "|.");
        options->off_o = 1;
      }
    }
    fclose(f);
  }
}

void flagsGrepO(char *line, regex_t *regex) {
  regmatch_t match = {0};
  while (regexec(regex, line, 1, &match, 0) == 0) {
    for (int x = match.rm_so; x < match.rm_eo; x++) {
      putchar(line[x]);
      for (int j = x; line[j] != '\0'; j++) {
        line[j] = line[j + 1];
      }
      x--;
      match.rm_eo--;
    }
    putchar('\n');
  }
}