#!/bin/bash
expression=""
result=""

flags=("-e" "-i" "-v" "-c" "-l" "-n" "-h" "-s" "-o" "-f")
flags_without=("-i" "-v" "-c" "-l" "-n" "-h" "-s" "-o")
files=("test1.txt" "test2.txt" "test3.txt" "test4.txt" "test5.txt" "s21_grep.c" "no_file.txt")
paterns=("patern.txt" "test1.txt" "no_file.txt")
reg_exp=("\"[A-C]\"" "\"[1-9]\"" "^int" "roma" "amir" "...ions" ".")
reg_int=1;
reg_end=5;

current_number=1
fail_number=0;
succes_number=0;

#[current_number|succes|fail] commands result;

tests without flags
echo "Tests without flags"
for i in ${reg_exp[@]}; do
    for j in ${files[@]}; do
        ./s21_grep $i $j > output.txt
        grep $i $j > output_grep.txt
        expression="$(diff output.txt output_grep.txt)"
        if [ "$expression" = "" ]
        then
            result="Success"
            succes_number=$(($succes_number + 1))
        else
            result="Fail"
            fail_number=$(($fail_number + 1))
        fi
        echo "[$current_number|$succes_number|$fail_number] $result grep $i $j;"
        current_number=$(($current_number + 1))
    done
done

echo ""

#Tests with flags
echo "Tests with flags"
for i in ${reg_exp[@]}; do
    for j in ${flags[@]}; do
        for k in ${files[@]}; do
            ./s21_grep $j $i $k > output.txt
            grep $j $i $k > output_grep.txt
            expression="$(diff output.txt output_grep.txt)"
            if [ "$expression" = "" ]
            then
                result="Success"
                succes_number=$(($succes_number + 1))
            else
                result="Fail"
                fail_number=$(($fail_number + 1))
            fi
            echo "[$current_number|$succes_number|$fail_number] $result grep $j $i $k;"
            current_number=$(($current_number + 1))
        done
    done
done

echo ""

#Tests with flag -e and 1< parameters
echo "Tests with flag -e"
for i in ${reg_exp[@]:0:5}; do 
    for j in ${reg_exp[@]:$reg_int:$reg_end}; do
        for k in ${files[@]}; do
            for m in ${flags_without_f[@]}; do
                ./s21_grep -e $i -e $j $k $m > output.txt
                grep -e $i -e $j $k $m > output_grep.txt
                expression="$(diff output.txt output_grep.txt)"
                if [ "$expression" = "" ]
                then
                    result="Success"
                    succes_number=$(($succes_number + 1))
                else
                    result="Fail"
                    fail_number=$(($fail_number + 1))
                fi
                echo "[$current_number|$succes_number|$fail_number] $result grep -e $i -e $j $k $m"
                current_number=$(($current_number + 1))
            done
        done
    done
    reg_int=$(($reg_int + 1))
    reg_end=$(($reg_end - 1))
done

echo ""

reg_int=1
reg_end=9
# multiflags tests
echo "Tests with multiflags"
for i in ${flags[@]:0:8}; do
    for j in ${flags[@]:reg_int:reg_end}; do
        for k in ${files[@]}; do
            for m in ${reg_exp[@]}; do
                # ./s21_grep -flag -flag pattern file > output.txt
                ./s21_grep $i $j $m $k > output.txt
                grep $i $j $m $k > output_grep.txt
                expression="$(diff output.txt output_grep.txt)"
                if [ "$expression" = "" ]
                then
                    result="Success"
                    succes_number=$(($succes_number + 1))
                else
                    result="Fail"
                    fail_number=$(($fail_number + 1))
                fi
                echo "[$current_number|$succes_number|$fail_number] $result grep $i $j $m $k"
                current_number=$(($current_number + 1))
            done
        done
    done
    reg_int=$(($reg_int + 1))
    reg_end=$(($reg_end - 1))
done

echo ""

#tests with flag f
echo "Tests with f flag"
for i in ${files[@]}; do
    for j in ${paterns[@]}; do    
        ./s21_grep -f $j $i > output.txt
        grep -f $j $i > output_grep.txt
        expression="$(diff output.txt output_grep.txt)"
        if [ "$expression" = "" ]
        then
            result="Success"
            succes_number=$(($succes_number + 1))
        else
            result="Fail"
            fail_number=$(($fail_number + 1))
        fi
        echo "[$current_number|$succes_number|$fail_number] $result grep -f $j $i"
        current_number=$(($current_number + 1))
    done
done

echo ""

#tests with flag f and other flags
echo "Tests with f flag and other flags"
for i in ${files[@]}; do
    for j in ${paterns[@]}; do    
     for k in ${flags_without[@]}; do
            ./s21_grep -f $j $i $k > output.txt
            grep -f $j $i $k > output_grep.txt
            expression="$(diff output.txt output_grep.txt)"
            if [ "$expression" = "" ]
            then
                result="Success"
                succes_number=$(($succes_number + 1))
            else
                result="Fail"
                fail_number=$(($fail_number + 1))
            fi
            echo "[$current_number|$succes_number|$fail_number] $result grep -f $j $i $k"
            current_number=$(($current_number + 1))
        done
    done
done

echo "All: $(($current_number - 1)) | Success: $succes_number | Fail: $fail_number"

rm -rf output_grep.txt
rm -rf output.txt 
