#ifndef s21_grep
#define s21_grep
#define MAXLINE 1024

#include <getopt.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct options {
  int e;
  int i;
  int v;
  int c;
  int l;
  int n;
  int f;
  int h;
  int s;
  int o;
  int off_o;
} opt;

void grepHandleFile(int argc, char *argv[], char *pattern, opt *options);
void grepParseArg(int argc, char *argv[], opt *options, char *pattern);
void flagsGrepE(char *optarg, char *pattern, opt *options);
void flagsGrepF(char *pattern, opt *options);
void flagsGrepO(char *line, regex_t *regex);
int compileRegex(char *pattern, opt *options, regex_t *regex);
void processFile(char *filename, opt *options, regex_t *regex, int files);
int compareFinding(regex_t *regex, opt *options, char *line);

#endif